# -*- coding: utf-8 -*-
"""

@author: Tina Minel/ Lea Foucher

"""

import random


print("\n Hello welcome to the Hangman game !\n To play you need to choose the difficulty you want.")
level=""

def intro():
    global level
    level= input("\n Choose from 1 (the easiest) to 5 (the hardest): ")
    if level in "12345":
        jeu()
        
    else:
        print(" You have to put a number betwen 1 to 5")
        intro()
   
    
    
def jeu():
    #choisi un mot
    if level=="1":
        mots = []
        with open("level_1.txt") as fl:
            for l in fl:
                mots.append(l.rstrip("\n"))
        mot = random.choice(mots)
        
    if level=="2":
        mots = []
        with open("level_2.txt") as fl:
            for l in fl:
                mots.append(l.rstrip("\n"))
        mot = random.choice(mots)
    
    if level=="3":
        mots = []
        with open("level_3.txt") as fl:
            for l in fl:
                mots.append(l.rstrip("\n"))
        mot = random.choice(mots)
        
    if level=="4":
        mots = []
        with open("level_4.txt") as fl:
            for l in fl:
                mots.append(l.rstrip("\n"))
        mot = random.choice(mots)
        
    if level=="5":
        mots = []
        with open("level_5.txt") as fl:
            for l in fl:
                mots.append(l.rstrip("\n"))
        mot = random.choice(mots)
    

    #variable cléf
    lettres = []
    faux= 0
    trouvé= False
    corp_plein =["☺","/","║","\\", "/","\\"]
    corp=[" "," "," "," "," "," "]

    #end=" " remplace le caractere \n par un espace
    while not trouvé:
        trouvé = True
        print("  ▄▬▬▬▄")
        print("  ║   |")
        print("  ║   {}".format(corp[0]))
        print("  ║  {}{}{}".format(corp[1], corp[2], corp[3]))
        print("  ║  {} {}".format(corp[4],corp[5]))
        print("  ║  ")
        print("_▄█▄_")
        print(" ")
        
        for l in mot:
            if l in lettres:
                print(l, end=" ")
            else:
                trouvé=False
                print("_", end=" ")
        
        print()
        print("\n Letters you already used - ",end="")
        for l in lettres:
            print(l, end=" | ")
        print()
        
        
        if faux > 5:
            print("\n You lost.")
            print(" The word was {}".format(mot))
            intro()
            
        if trouvé:
            print("\n You won!")
            intro()
            break
        
        lettre = input(" Type a letter : ")
        lettre=lettre.upper()
        if lettre in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
            lettres.append(lettre)
        else:
            print("\n\n\n\n Type a letter please\n")
            lettres.append(lettre)
        
        if lettre not in mot:
            corp[faux] = corp_plein[faux]
            faux += 1
            
intro()